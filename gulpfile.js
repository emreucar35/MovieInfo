var gulp = require('gulp');
var elm  = require('gulp-elm');
var uglify = require('gulp-uglify');
var stylus = require('gulp-stylus');
var concat = require('gulp-concat');
var mergeStream = require('merge-stream');
var pump = require('pump');

var distDir = 'dist/';

gulp.task('static', function () {
    return gulp.src(['src/*.html', 'src/favicon.png', 'src/opensearchdescription.xml'])
        .pipe(gulp.dest(distDir));
});

function bundleCss (compressed) {
    var cssStream = gulp.src('src/bootstrap.min.css');

    var stylusStream = pump([
        gulp.src('src/main.styl'),
        stylus({compress: compressed})
    ]);        

    return mergeStream(cssStream, stylusStream)
        .pipe(concat('app.css'))
        .pipe(gulp.dest(distDir));
}

gulp.task('css', function () {
    return bundleCss(false);
});

gulp.task('css-compressed', function () {
    return bundleCss(true);
});

function bundleJs (compressed) {
    var jsStream = pump([
        gulp.src('src/*.elm'),
        elm.bundle('app.js'),
    ]);

    if (compressed)
        jsStream = jsStream.pipe(uglify());

    return jsStream.pipe(gulp.dest(distDir));
}

gulp.task('js', function (cb) {
    return bundleJs(false);
});

gulp.task('js-compressed', function () {
    bundleJs(true);
});

gulp.task('watch', function () {
    gulp.watch(['src/*.html', 'favicon.png', 'src/opensearchdescription.xml'], ['static']);
    gulp.watch('src/*.styl', ['css']);
    gulp.watch('src/*.elm', ['js']);
});
             
gulp.task('build', ['static', 'css-compressed', 'js-compressed']);

gulp.task('default', ['static', 'css', 'js', 'watch']);